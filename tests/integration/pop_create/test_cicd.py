import os
import pathlib
import tempfile
from unittest import mock

from tests.integration.conftest import compare_dir_trees


def test_cli(hub):
    with tempfile.TemporaryDirectory(prefix="test_", suffix="_pop_create") as temp_dir:
        temp_path = pathlib.Path(temp_dir)
        # Verify that the temporary directory is completely empty
        assert not os.listdir(temp_dir)

        # Run the pop-create cli with mocked arguments
        with mock.patch(
            "sys.argv",
            [
                "pop-create",
                "cicd",
                "--overwrite",
                "--vertical",
                f"--directory={temp_dir}",
                "--project-name=test-project",
            ],
        ):
            hub.pop_create.init.cli()

        compare_dir_trees(
            os.walk(temp_dir),
            [
                (
                    temp_dir,
                    [],
                    [
                        "noxfile.py",
                        ".pre-commit-config.yaml",
                        ".gitlab-ci.yml",
                        ".coveragerc",
                        "build.conf",
                    ],
                ),
            ],
        )
